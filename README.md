# Bebop Flight App  
Control drone and can view FPV in PC via nodejs.  
Windows and Unix platform are supported.  
  
# Installation  
Read INSTALL.md  
You can open Markup file in notepad.exe  
  
# How to run  
1.connect ps3 controller via sixaxis  
2.connect to bebop drone  
3.execute the command via any commandline(bash,dash,sh,zsh,csh,DOS,powershell)  
  node index.js   



#!/usr/bin/env python
import sys
import matplotlib.pyplot as plt
dat_file = open(sys.argv[1]+".txt","r").read()
dat_file = dat_file.split("\n")
x = [i+1 for i in range(len(dat_file)-1)]
y = [float(i.replace(" ","")) for i in dat_file if i is not ""]
plt.plot(x,y,'o')
plt.savefig(sys.argv[1]+".png")
